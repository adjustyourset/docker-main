# base image
FROM frolvlad/alpine-glibc:latest

MAINTAINER Jakub Kwasiborski "jakub@adjustyourset.com"

# set timezone
ENV TIMEZONE Europe/London
RUN apk add --no-cache tzdata
RUN ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime

# Install GraphicsMagick
#ENV PKGNAME=graphicsmagick
#ENV PKGVER=1.3.23
#ENV PKGSOURCE=http://downloads.sourceforge.net/$PKGNAME/$PKGNAME/$PKGVER/GraphicsMagick-$PKGVER.tar.lz

# RUN apk add --update graphicsmagick --update-cache --repository http://dl-3.alpinelinux.org/alpine/edge/testing/ --allow-untrusted
#
# Installing graphicsmagick dependencies
#RUN apk add --update g++ \
#                     gcc \
#                     make \
#                     lzip \
#                     wget \
#                     libjpeg-turbo-dev \
#                     libpng-dev \
#                     libtool \
#                     libgomp && \
#    wget $PKGSOURCE && \
#    lzip -d -c GraphicsMagick-$PKGVER.tar.lz | tar -xvf - && \
#    cd GraphicsMagick-$PKGVER && \
#    ./configure \
#      --build=$CBUILD \
#      --host=$CHOST \
#      --prefix=/usr \
#      --sysconfdir=/etc \
#      --mandir=/usr/share/man \
#      --infodir=/usr/share/info \
#      --localstatedir=/var \
#      --enable-shared \
#      --disable-static \
#      --with-modules \
#      --with-threads \
#      --with-gs-font-dir=/usr/share/fonts/Type1 \
#      --with-quantum-depth=16 && \
#    make && \
#    make install && \
#    cd / && \
#    rm -rf GraphicsMagick-$PKGVER && \
#    rm GraphicsMagick-$PKGVER.tar.lz && \
#    apk del g++ \
#            gcc \
#            make \
#            lzip \
#            wget

# install node, npm, imagemagick, mysql-client, and 
# npm modules : forever grunt-cli bower pac nodemon mocha node-inspector browserify harp
ENV VERSION=v6.9.4 NPM_VERSION=3

RUN apk add --no-cache curl make gcc g++ python linux-headers paxctl libgcc libstdc++ gnupg python-dev py-pip mysql-client imagemagick git && \
  gpg --keyserver pool.sks-keyservers.net --recv-keys 9554F04D7259F04124DE6B476D5A82AC7E37093B && \
  gpg --keyserver pool.sks-keyservers.net --recv-keys 94AE36675C464D64BAFA68DD7434390BDBE9B9C5 && \
  gpg --keyserver pool.sks-keyservers.net --recv-keys 0034A06D9D9B0064CE8ADF6BF1747F4AD2306D93 && \
  gpg --keyserver pool.sks-keyservers.net --recv-keys FD3A5288F042B6850C66B31F09FE44734EB7990E && \
  gpg --keyserver pool.sks-keyservers.net --recv-keys 71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 && \
  gpg --keyserver pool.sks-keyservers.net --recv-keys DD8F2338BAE7501E3DD5AC78C273792F7D83545D && \
  gpg --keyserver pool.sks-keyservers.net --recv-keys C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 && \
  gpg --keyserver pool.sks-keyservers.net --recv-keys B9AE9905FFD7803F25714661B63B535A4C206CA9 && \
  curl -o node-${VERSION}.tar.gz -sSL https://nodejs.org/dist/${VERSION}/node-${VERSION}.tar.gz && \
  curl -o SHASUMS256.txt.asc -sSL https://nodejs.org/dist/${VERSION}/SHASUMS256.txt.asc && \
  gpg --verify SHASUMS256.txt.asc && \
  grep node-${VERSION}.tar.gz SHASUMS256.txt.asc | sha256sum -c - && \
  tar -zxf node-${VERSION}.tar.gz && \
  cd node-${VERSION} && \
  export GYP_DEFINES="linux_use_gold_flags=0" && \
  ./configure --prefix=/usr ${CONFIG_FLAGS} && \
  NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) && \
  make -j${NPROC} -C out mksnapshot BUILDTYPE=Release && \
  paxctl -cm out/Release/mksnapshot && \
  make -j${NPROC} && \
  make install && \
  paxctl -cm /usr/bin/node && \
  cd / && \
  if [ -x /usr/bin/npm ]; then \
    npm install -g npm@${NPM_VERSION} && \
    find /usr/lib/node_modules/npm -name test -o -name .bin -type d | xargs rm -rf; \
  fi && \
  npm install -g forever \
                 grunt-cli \
                 bower \
                 aystech/node-pac \
                 nodemon \
                 mocha \
                 node-inspector \
                 browserify \
#                 harp \   #  https://github.com/sintaxi/harp/issues/556
                 gulp \
                 node-static && \
  npm cache clean && \
  rm -rf /tmp/npm-* && \
  apk del curl make gcc g++ linux-headers paxctl gnupg ${DEL_PKGS} git pcre && \
  rm -rf /etc/ssl /node-${VERSION}.tar.gz /SHASUMS256.txt.asc /node-${VERSION} ${RM_DIRS} \
    /usr/share/man /tmp/* /var/cache/apk/* /root/.npm /root/.node-gyp /root/.gnupg \
    /usr/lib/node_modules/npm/man /usr/lib/node_modules/npm/doc /usr/lib/node_modules/npm/html && \
  pip install virtualenv && \
  rm -rf /var/cache/apk/*

# raise open file descriptors limits
# ADD ./limits.conf /etc/security/limits.conf
